output "mysql_ip" {
  description = "Public IP address Mysql"
  value       = aws_db_instance.mysql.endpoint
}

output "domain_name" {
  description = "Elb domain name"
  value       = module.elb_http.this_elb_dns_name
}
