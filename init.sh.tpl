#!/bin/bash


curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"

sudo dpkg -i session-manager-plugin.deb

apt update && apt install -y apache2 php libapache2-mod-php php-mysql

index_php_base64="PD9waHAKJGNvbm4gPSBuZXcgbXlzcWxpKCdfSE9TVF8nLCAnX1VTRVJOQU1FXycsICdfUEFTU1dPUkRfJyk7CmlmICgkY29ubi0+Y29ubmVjdF9lcnJvcikgewplY2hvICgnS086ICcgLiAkY29ubi0+Y29ubmVjdF9lcnJvcik7Cn0KZWNobyAnT0snCj8+Cgo="

echo $index_php_base64 | base64 -d | \
    sed "s|_HOST_|${host}| ;s|_USERNAME_|${username}|; s|_PASSWORD_|${password}|" \
    > /var/www/html/index.php

echo "Done"
