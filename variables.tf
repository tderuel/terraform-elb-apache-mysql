variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "eu-west-1"
}
variable "owname" {
  description = "Owner name"
  type        = string
}

