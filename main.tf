terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

locals {
  common_tags = {
    Terraform   = "true"
    Owner       = var.owname
    Environment = "dev"
  }
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

# Module VPC
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "infra-web-vpc-${var.owname}"
  cidr = "10.0.0.0/16"

  azs             = ["${var.aws_region}a", "${var.aws_region}b", "${var.aws_region}c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true

  tags = local.common_tags
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow http inbound traffic"
  #vpc_id      = module.vpc.vpc_id
  vpc_id = data.aws_vpc.default.id
  ingress = [
    {
      description = "HTTP from VPC"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      #cidr_blocks      = [module.vpc.vpc_cidr_block]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      self             = false
      security_groups  = []
    }
  ]
  egress = [
    {
      description      = "ALLOW ALL"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      self             = false
      security_groups  = []

    }
  ]
  tags = local.common_tags
}
resource "aws_security_group" "allow_mysql" {
  name        = "Allow mysql"
  description = "Allow mysql inbound traffic"
  #vpc_id      = module.vpc.vpc_id
  vpc_id = data.aws_vpc.default.id
  ingress = [
    {
      description = "mysql from VPC"
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      #cidr_blocks      = [module.vpc.vpc_cidr_block]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      self             = false
      security_groups  = []
    }
  ]
  egress = [
    {
      description      = "ALLOW ALL"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      self             = false
      security_groups  = []

    }
  ]
  tags = local.common_tags
}

# Module ELB
module "elb_http" {
  source  = "terraform-aws-modules/elb/aws"
  version = "~> 2.0"

  name = "elb-infra-web-${var.owname}"

  #subnets         = module.vpc.private_subnets
  subnets         = data.aws_subnet_ids.all.ids
  security_groups = [aws_security_group.allow_http.id]
  internal        = false

  listener = [
    {
      instance_port     = 80
      instance_protocol = "HTTP"
      lb_port           = 80
      lb_protocol       = "HTTP"
    }
  ]

  health_check = {
    target              = "HTTP:80/"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  #  access_logs = {
  #    bucket = "my-access-logs-bucket"
  #  }

  #  // ELB attachments
  number_of_instances = length(aws_instance.web)
  instances           = aws_instance.web[*].id

  tags = local.common_tags
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  count                  = 2
  user_data              = templatefile("init.sh.tpl", { host = aws_db_instance.mysql.endpoint, username = aws_db_instance.mysql.username, password = aws_db_instance.mysql.password })
  vpc_security_group_ids = [aws_security_group.allow_http.id]
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t3.micro"

  tags = merge(
    local.common_tags,
    {
      Name = "HelloWorld"
    },
  )
}

resource "aws_db_instance" "mysql" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  name                 = "mydb"
  username             = "foo"
  password             = "foobarbaz"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
  vpc_security_group_ids = [aws_security_group.allow_mysql.id]
}


